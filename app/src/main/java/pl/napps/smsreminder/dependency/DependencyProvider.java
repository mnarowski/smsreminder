package pl.napps.smsreminder.dependency;

import java.util.Hashtable;

/**
 * Created by mnarowski on 2016-05-03.
 */
public class DependencyProvider {

    private static Hashtable<Class,Object> sMap = new Hashtable<>();

    public static void inject(Class klass, Object impl){
        sMap.put(klass,impl);
    }

    public static <T> T provide(Class<T> klass){
        return (T) sMap.get(klass);
    }
}

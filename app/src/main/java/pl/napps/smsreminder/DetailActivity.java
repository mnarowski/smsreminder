package pl.napps.smsreminder;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.QuickContactBadge;
import android.widget.TimePicker;

import com.android.datetimepicker.date.MonthAdapter;

import java.sql.SQLException;
import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import pl.napps.smsreminder.db.DbHelper;
import pl.napps.smsreminder.model.Event;

import static pl.napps.smsreminder.dependency.DependencyProvider.provide;

public class DetailActivity extends AppCompatActivity {

    public static final String EVENT = Event.class.getSimpleName();
    private static final long DAY = 24 * 3600 * 1000L;


    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.fab)
    FloatingActionButton fab;
    private Unbinder mUnbinder;
    private Event mEvent;

    @BindView(R.id.event_message)
    EditText message;
    @BindView(R.id.event_day)
    DatePicker datePicker;

    @BindView(R.id.event_time)
    TimePicker picker;
    @BindView(R.id.event_contact)
    QuickContactBadge contactBadge;
    @BindView(R.id.date_label)
    View dateLabel;

    @BindView(R.id.time_label)
    View timeLabel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        mUnbinder = ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        if (savedInstanceState==null){
            mEvent = (Event) getIntent().getSerializableExtra(EVENT);
        }

        if (mEvent!=null){
            message.setText(mEvent.message);
            if (mEvent.when > 0L){
                Calendar calendar = Calendar.getInstance();
                calendar.setTimeInMillis(mEvent.when);
                datePicker.updateDate(calendar.get(Calendar.YEAR),calendar.get(Calendar.MONTH),calendar.get(Calendar.DAY_OF_MONTH));

            }
            if (mEvent.linkedContactId>0L){
                //getContact
            }
        }


    }

    @OnClick({R.id.date_label})
    public void onDate(View view){
        if (datePicker.getVisibility() == View.GONE){
          showView(datePicker);
        }
        else{
            hideView(datePicker);
        }
    }


    @OnClick({R.id.time_label})
    public void onTime(View view){
        if (picker.getVisibility() == View.GONE){
            showView(picker);
        }
        else{
            hideView(picker);
        }
    }

    private void hideView(final View view) {
       view.animate().alpha(0.0f).translationX(view.getWidth()).setDuration(500).setListener(new AnimatorListenerAdapter() {
           @Override
           public void onAnimationEnd(Animator animation) {
               super.onAnimationEnd(animation);
               view.setVisibility(View.GONE);
           }
       });
    }

    private void showView(final View view) {
        view.animate().alpha(1.0f).translationX(0).setDuration(500).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationStart(Animator animation) {
                super.onAnimationStart(animation);
                view.setVisibility(View.VISIBLE);
            }
        });
    }

    @OnClick({R.id.fab})
    public void save(View view){
        String message = this.message.getText().toString();
        mEvent.message = message;
        //implement
        mEvent.when = System.currentTimeMillis() + DAY;
        try {
            provide(DbHelper.class).getModelDao(Event.class).createOrUpdate(mEvent);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mUnbinder!=null){
            mUnbinder.unbind();
            mUnbinder = null;
        }
    }
}

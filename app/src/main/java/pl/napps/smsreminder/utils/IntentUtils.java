package pl.napps.smsreminder.utils;

import android.content.Context;
import android.content.Intent;

import pl.napps.smsreminder.DetailActivity;
import pl.napps.smsreminder.model.Event;

/**
 * Created by mnarowski on 2016-05-05.
 */
public class IntentUtils {

    public static void showDetails(Context context, Event event){
        Intent intent = new Intent(context, DetailActivity.class);
        intent.putExtra(DetailActivity.EVENT,event);
        context.startActivity(intent);
    }
}

package pl.napps.smsreminder;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import pl.napps.smsreminder.adapters.EventsAdapter;
import pl.napps.smsreminder.db.DbHelper;
import pl.napps.smsreminder.log.Logger;
import pl.napps.smsreminder.model.Event;
import pl.napps.smsreminder.utils.IntentUtils;
import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

import static pl.napps.smsreminder.dependency.DependencyProvider.provide;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getSimpleName();
    @BindView(R.id.events_swipe)
    SwipeRefreshLayout swipe;

    @BindView(R.id.events_list)
    RecyclerView eventList;

    @BindView(R.id.fab)
    FloatingActionButton fab;

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    private Unbinder mUnbider;
    private Subscription mSubsciptions;
    private EventsAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mUnbider = ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        if (savedInstanceState == null){
            loadData(false);
            mAdapter = new EventsAdapter();
        }
        swipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadData(true);
            }
        });
        eventList.setLayoutManager(new LinearLayoutManager(this));
        eventList.setAdapter(mAdapter);

    }
    @OnClick({R.id.fab})
    public void createEvent(View view){
        IntentUtils.showDetails(this,new Event());
    }

    private void loadData(boolean clear) {
        mSubsciptions = Observable.create(getEvents()).subscribeOn(Schedulers.newThread()).observeOn(AndroidSchedulers.mainThread()).subscribe(onLoaded(clear),onError());
    }

    private Action1<? super List<Event>> onLoaded(final boolean clear) {
        return new Action1<List<Event>>() {
            @Override
            public void call(List<Event> events) {
                if (clear){
                    mAdapter.clear();
                }
                mAdapter.addItems(events);
            }
        };
    }

    private Action1<Throwable> onError() {
        return new Action1<Throwable>() {
            @Override
            public void call(Throwable throwable) {
                Logger.e(TAG,throwable.getMessage(),throwable);
            }
        };
    }

    private Observable.OnSubscribe<List<Event>> getEvents() {
        return new Observable.OnSubscribe<List<Event>>() {
            @Override
            public void call(Subscriber<? super List<Event>> subscriber) {
                 try{
                     List<Event> list= (List<Event>) provide(DbHelper.class).getModelDao(Event.class).queryForAll();
                     subscriber.onNext(list);
                     subscriber.onCompleted();
                 }catch (Throwable t){
                     subscriber.onError(t);
                 }
            }
        };
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(mSubsciptions!=null){
            mSubsciptions.unsubscribe();
            mSubsciptions = null;
        }
        if (mUnbider!=null){
            mUnbider.unbind();
            mUnbider = null;
        }
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_add){
            IntentUtils.showDetails(this,new Event());
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}

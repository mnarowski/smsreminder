package pl.napps.smsreminder.log;

import java.sql.SQLException;

import pl.napps.smsreminder.BuildConfig;

/**
 * Created by mnarowski on 2016-05-03.
 */
public class Logger {

    public static void e(String tag, String message, Throwable e) {
        if(BuildConfig.ALLOW_LOGS){
            android.util.Log.e(tag,message,e);
        }
    }


    public static void e(String tag, String message) {
        if(BuildConfig.ALLOW_LOGS){
            android.util.Log.e(tag,message);
        }
    }
}

package pl.napps.smsreminder.app;

import android.app.Application;

import org.greenrobot.eventbus.EventBus;

import pl.napps.smsreminder.db.DbHelper;
import pl.napps.smsreminder.db.DbHelperImpl;

import static pl.napps.smsreminder.dependency.DependencyProvider.inject;

/**
 * Created by mnarowski on 2016-05-03.
 */
public class SMSReminderApp extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        initDependencies();
    }

    private void initDependencies() {
        inject(DbHelper.class, DbHelperImpl.getInstance(this));
        inject(EventBus.class,EventBus.getDefault());
    }

}

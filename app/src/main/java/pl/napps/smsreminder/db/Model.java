package pl.napps.smsreminder.db;

/**
 * Created by mnarowski on 2016-05-03.
 */
public interface Model {
    long getId();
}

package pl.napps.smsreminder.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.io.File;
import java.io.InputStream;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Set;
import java.util.TreeSet;

import pl.napps.smsreminder.R;
import pl.napps.smsreminder.log.Logger;
import pl.napps.smsreminder.model.Event;

/**
 * Created by mnarowski on 2016-05-03.
 */
public class DbHelperImpl extends OrmLiteSqliteOpenHelper implements DbHelper {
    private static final String DEFAULT_DB_NAME = "sms_reminder.events.db";
    private static final int DEFAULT_DB_VERSION = 1;
    private static final String TAG = DbHelperImpl.class.getSimpleName();
    private static DbHelperImpl sInstance;
    private HashMap<Class,Dao> sDao  = new HashMap<>();//= new TreeSet<>();

    public DbHelperImpl(Context context, String databaseName, SQLiteDatabase.CursorFactory factory, int databaseVersion) {
        super(context, databaseName, factory, databaseVersion);
    }

    public DbHelperImpl(Context context, String databaseName, SQLiteDatabase.CursorFactory factory, int databaseVersion, int configFileId) {
        super(context, databaseName, factory, databaseVersion, configFileId);
    }

    public DbHelperImpl(Context context, String databaseName, SQLiteDatabase.CursorFactory factory, int databaseVersion, File configFile) {
        super(context, databaseName, factory, databaseVersion, configFile);
    }

    public DbHelperImpl(Context context, String databaseName, SQLiteDatabase.CursorFactory factory, int databaseVersion, InputStream stream) {
        super(context, databaseName, factory, databaseVersion, stream);
    }

    public DbHelperImpl(Context context) {
        super(context,dbName(context),null,dbVersion(context));
    }

    private static String dbName(Context context) {
        return context.getResources().getString(R.string.db_name);
    }

    private static int dbVersion(Context context) {
        return context.getResources().getInteger(R.integer.db_version);
    }

    public static DbHelper getInstance(Context context){
        if (sInstance ==null){
            sInstance = new DbHelperImpl(context);
        }
        return sInstance;
    }

//    @Override
//    public Dao getModelDao(Class klass) throws SQLException {
//        return getDao(klass);
//    }




    @Override
    public <T extends Model> Dao<T, Long> getModelDao(Class<T> klass) throws SQLException {
        return createOrGet(klass);
    }

    private <T extends Model> Dao<T, Long> createOrGet(Class<T> klass) throws SQLException {
        if (sDao.containsKey(klass)){
            sDao.put(klass.getComponentType(),getDao(klass));
        }
        return (Dao<T, Long>) sDao.get(klass.getComponentType());
    }

    @Override
    public void onCreate(SQLiteDatabase database, ConnectionSource connectionSource) {
        try {
            TableUtils.createTableIfNotExists(connectionSource, Event.class);
        } catch (SQLException e) {
            Logger.e(TAG,e.getMessage(),e);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase database, ConnectionSource connectionSource, int oldVersion, int newVersion) {

    }
}

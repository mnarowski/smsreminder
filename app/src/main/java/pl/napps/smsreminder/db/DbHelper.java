package pl.napps.smsreminder.db;

import com.j256.ormlite.dao.Dao;

import java.sql.SQLException;

/**
 * Created by mnarowski on 2016-05-03.
 */
public interface DbHelper {
//    Dao getModelDao(Class klass) throws SQLException;
    <T extends Model> Dao<T, Long> getModelDao(Class<T> klass) throws SQLException;
}

package pl.napps.smsreminder.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;

import pl.napps.smsreminder.db.Model;

/**
 * Created by mnarowski on 2016-05-03.
 */
@DatabaseTable(tableName = "EVENTS")
public class Event implements Model,Serializable {

    @DatabaseField(id = true)
    public long id;
    @DatabaseField
    public String message;
    @DatabaseField
    public long when;
    @DatabaseField
    public long linkedContactId;

    @Override
    public long getId() {
        return id;
    }
}

package pl.napps.smsreminder.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import pl.napps.smsreminder.R;
import pl.napps.smsreminder.model.Event;
import pl.napps.smsreminder.utils.IntentUtils;

/**
 * Created by mnarowski on 2016-05-03.
 */
public class EventsAdapter extends RecyclerView.Adapter<EventVH> {

    private final ArrayList<Event> mItems;

    public EventsAdapter(){
        mItems = new ArrayList<Event>();
    }

    @Override
    public EventVH onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = null;
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_item,parent,false);
        return new EventVH(view);
    }

    @Override
    public void onBindViewHolder(EventVH holder, int position) {
        Event item = mItems.get(position);
        updateItem(holder,item);
    }

    private void updateItem(EventVH holder, final Event item) {
        holder.title.setText(item.message);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                IntentUtils.showDetails(v.getContext(),item);
            }
        });
    }

    public void addItems(List<Event> eventList){
        int count = mItems.size();
        mItems.addAll(eventList);
        notifyItemRangeChanged(count,eventList.size());
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    public void clear() {
        mItems.clear();
        notifyDataSetChanged();
    }
}

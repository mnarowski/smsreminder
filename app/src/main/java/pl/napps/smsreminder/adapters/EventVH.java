package pl.napps.smsreminder.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import pl.napps.smsreminder.R;

/**
 * Created by mnarowski on 2016-05-03.
 */
public class EventVH extends RecyclerView.ViewHolder {

    @BindView(R.id.event_message)
    TextView title;

    public EventVH(View itemView) {
        super(itemView);
        ButterKnife.bind(this,itemView);
    }
}
